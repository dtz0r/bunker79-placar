package com.bunker79.placar.api.vo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ArticleResponse {

	private String titulo;
	private int total_resultados;
	private int itens_por_pagina;
	private int pagina_atual;
	private ArticleVO[] resultado;
	
	
	public String getTitulo() {
		return titulo;

	}
	
	public ArticleVO[] getArticles () {
		return resultado;
	}

	
}
