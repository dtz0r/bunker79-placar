package com.bunker79.placar.api.vo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MatchVO {
	
	private String rodada;
	private String grupo;
	private String grupo_time1;
	private String grupo_time2;
	private String data;
	private String hora;
	private String local;
	private String estadio;
	private Boolean iniciado;
	private Boolean terminado;
	private TeamVO time_casa;
	private TeamVO time_visitante;
	private String gols_time_casa;
	private String gols_time_visitante;
	private String penaltis_time_casa;
	private String penaltis_time_visitante;
	private MatchLinkVO[] link;
	
	
	public TeamVO getHomeTeam() {
		return time_casa;
	}

	public TeamVO getVisitorTeam() {
		return time_visitante;
	}
	
	public String getHomeGoals() {
		return gols_time_casa;
	}
	
	public String getVisitorGoals() {
		return gols_time_visitante;
	}
	
	public String getMatchResult(){
		
		StringBuilder sb = new StringBuilder();
		
		sb.append(getHomeGoals());
		sb.append(" X ");
		sb.append(getVisitorGoals());
		
		return sb.toString();
	}
	
	public String getDate() {
		
		StringBuilder sb = new StringBuilder();	
		sb.append(data);
		sb.append(" �s ");
		sb.append(hora);
		
		return sb.toString();
	}
	
	public CharSequence getFormatedData() throws ParseException {

		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM �'s' HH:mm");

		return sdf.format(getDate());
	}
	
	public String getMatchTime() {
		return hora;
	}
	
	public String getStadium() {
		return estadio;
	}
	

}
