package com.bunker79.placar.api.vo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;



public class ArticleVO {

	private String titulo;
	private String subtitulo;
	private String slug;
	private String data_disponibilizacao;
	private String recurso_url;
	private ArticleImageVO imagem;
	private ArticleLinkVO links[];


	public String getTitulo() {
		return titulo;
	}

	public String getSlug() {
		return slug;
	}
	
	public Date getDate() throws ParseException {

		SimpleDateFormat parser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss-SS:SS");

		return parser.parse(data_disponibilizacao);

	}
	
	public CharSequence getFormatedData() throws ParseException {

		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM �'s' HH:mm");

		return sdf.format(getDate());
	}

	public String getArticleUrl() {

		return recurso_url;
	}


}
