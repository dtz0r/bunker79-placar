package com.bunker79.placar.api.vo;


public class MatchesResponse {
	
	private String titulo;
	private String campeonato;
	private MatchVO[] partidas;	
	private MatchLinkTransfVO[] links;	
	
	public String getTitulo() {
		return titulo;

	}
	
	public MatchVO[] getMatches () {
		return partidas;
	}
	
	public String getChampionship () {
		return campeonato;
	}
	
}
