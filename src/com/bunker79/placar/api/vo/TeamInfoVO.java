package com.bunker79.placar.api.vo;

public class TeamInfoVO {

	private String nome;
	private String slug;
	private String cor_primaria;
	private String cor_contraste;
	private TeamsShieldVO escudo;
	private ArticleVO[] resultado;
	private String treinador;
	private String descricao;
	private String federacao;
	private String titulos;
	private TeamLinkVO links[];
	
	public String getName() {
		return nome;
	}
	
	public String getCoach() {
		return treinador;
	}
	
	public String getChampionships() {
		return titulos;
	}


}
