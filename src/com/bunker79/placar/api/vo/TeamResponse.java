package com.bunker79.placar.api.vo;


public class TeamResponse {
	
	private String titulo;
	private int total_resultados;
	private int itens_por_pagina;
	private int pagina_atual;
	private TeamInfoVO[] resultado;
	
	
	public String getTitulo() {
		return titulo;

	}
	
	public TeamInfoVO[] getTeams () {
		return resultado;
	}
	
}
