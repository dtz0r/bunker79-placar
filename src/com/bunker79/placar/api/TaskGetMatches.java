package com.bunker79.placar.api;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;

import android.os.AsyncTask;

import com.bunker79.placar.Config;
import com.bunker79.placar.api.vo.MatchesResponse;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class TaskGetMatches extends AsyncTask<String, Void, MatchesResponse> {

	private String mURL = Config.API_METHOD_GET_MATCHES;

	private MatchesVOListener mListener;

	public void setListener(MatchesVOListener listener){

		mListener = listener;
	}
	
	public interface MatchesVOListener {

		public void eventosLoaded (MatchesResponse result);

	}

	@Override
	protected MatchesResponse doInBackground(String... arg0) {

		InputStream in;
		
		try {
			in = downloadUrl(mURL);
			Gson gson = new GsonBuilder().create();
			Reader reader = new InputStreamReader(in);

			MatchesResponse matchesResponse = gson.fromJson(reader, MatchesResponse.class);

			return matchesResponse;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

	@Override
	protected void onPostExecute(MatchesResponse result) {

		if(mListener != null){
			mListener.eventosLoaded(result);
			
		}
	}


	protected InputStream downloadUrl(String urlString) throws IOException {

		URL url = new URL(urlString);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setReadTimeout(25000);
		conn.setConnectTimeout(20000);

		conn.setRequestMethod("GET");
		conn.setRequestProperty("X-Placar-Token", Config.API_TOKEN);
		conn.setDoInput(true);
		conn.connect();
		InputStream stream = conn.getInputStream();
		return stream;
	}
	
	
	
}
