package com.bunker79.placar.api;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;

import android.os.AsyncTask;

import com.bunker79.placar.Config;
import com.bunker79.placar.api.vo.ArticleResponse;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class TaskGetArticles extends AsyncTask<String, Void, ArticleResponse> {

	private String mURL = Config.API_METHOD_GET_ARTICLES;

	private ArticlesVOListener mListener;

	public void setListener(ArticlesVOListener listener){

		mListener = listener;
	}
	
	public interface ArticlesVOListener {

		public void eventosLoaded (ArticleResponse result);

	}

	@Override
	protected ArticleResponse doInBackground(String... arg0) {

		InputStream in;
		
		try {
			in = downloadUrl(mURL);
			Gson gson = new GsonBuilder().create();
			Reader reader = new InputStreamReader(in);

			ArticleResponse articlesResponse = gson.fromJson(reader, ArticleResponse.class);


			return articlesResponse;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

	@Override
	protected void onPostExecute(ArticleResponse result) {

		if(mListener != null){
			mListener.eventosLoaded(result);
			
		}
	}


	protected InputStream downloadUrl(String urlString) throws IOException {

		URL url = new URL(urlString);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setReadTimeout(25000);
		conn.setConnectTimeout(20000);

		conn.setRequestMethod("GET");
		conn.setRequestProperty("X-Placar-Token", Config.API_TOKEN);
		conn.setDoInput(true);
		conn.connect();
		InputStream stream = conn.getInputStream();
		return stream;
	}
	
	
	
}
