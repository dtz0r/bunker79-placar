package com.bunker79.placar.dispatcher;

import com.bunker79.placar.api.vo.ArticleVO;


public class DataEvent extends SimpleEvent {
	
	public static final String ARTICLES_LOADED = "ARTICLE LOADED";
	public static final String TEAMS_LOADED = "TEAMS LOADED";
	public static final String MATCHES_LOADED = "MATCHES LOADED";
	
	private int mData;
	
	public DataEvent(String type, int data) {
		super(type);
		mData = data;
	}
	
	public DataEvent(String type, ArticleVO event) {
		super(type);
	}

	public int getData() {
		return mData;
	}
	

}
