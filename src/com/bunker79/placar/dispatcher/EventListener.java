package com.bunker79.placar.dispatcher;

public interface EventListener {
	
	void onEvent(Event event);

}
