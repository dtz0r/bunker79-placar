package com.bunker79.placar.dispatcher;

public interface Event {
	
	public String getType();
	public Object getSource();
	public void setSource(Object source);
}
