package com.bunker79.placar;

public class Config {
	
	public static final String API_TOKEN = "Token token=\"8625adda9058039971985eb1bbc4cff9d5bd2e7f1617fce3a1c1ed3dad87f74b62ac4f548f7f7dfd664acab6c45cedfcc657fb7ea7f59ac6ee58354aa59bfbd8\"";
	
	public static final String API_METHOD_GET_ARTICLES = "http://ext-api.placar.abril.com.br/campeonatos/copa-das-confederacoes/materias";
	
	public static final String API_METHOD_GET_ARTICLES_BY_TEAMS ="http://ext-api.placar.abril.com.br/selecoes/brasil/materias";
	
	public static final String API_METHOD_GET_ARTICLE_BY_SLUG ="http://ext-api.placar.abril.com.br/materias/";
	
	public static final String API_METHOD_GET_ARTICLE_FEATURED ="http://ext-api.placar.abril.com.br/materias/destaque";
	
	public static final String API_METHOD_GET_GALLERY ="http://ext-api.placar.abril.com.br/campeonatos/copa-das-confederacoes/galerias";
	
	public static final String API_METHOD_GET_GALLERY_BY_SLUG ="http://ext-api.placar.abril.com.br/galerias/";
	
	public static final String API_METHOD_GET_MATCHES ="http://ext-api.placar.abril.com.br/campeonatos/copa-das-confederacoes/partidas";
	
	public static final String API_METHOD_GET_TEAMS_POINTS ="http://ext-api.placar.abril.com.br/campeonatos/copa-das-confederacoes/tabela";
	
	public static final String API_METHOD_GET_TEAMS ="http://ext-api.placar.abril.com.br/selecoes";
	
}
