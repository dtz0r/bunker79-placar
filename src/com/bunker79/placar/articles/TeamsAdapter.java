package com.bunker79.placar.articles;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.bunker79.placar.R;
import com.bunker79.placar.api.vo.TeamInfoVO;

public class TeamsAdapter extends BaseAdapter {

	private Context mCtx;
	private ArrayList<TeamInfoVO> mSource;
	protected LayoutInflater fInflater;

	public TeamsAdapter(Context ctx, ArrayList<TeamInfoVO> teamsList) {
		mCtx = ctx;
		mSource = teamsList;

	}


	@Override
	public int getCount() {

		return mSource.size();
	}

	@Override
	public Object getItem(int position) {
		return mSource.get(position);
	}

	@Override
	public long getItemId(int position) {

		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		convertView = LayoutInflater.from(mCtx).inflate(R.layout.viewpagerteams, null);

		TextView textViewTeam = (TextView) convertView.findViewById(R.id.textViewTeam);
		TextView textViewCoach = (TextView) convertView.findViewById(R.id.textViewCoach);
		TextView textViewChampionship = (TextView) convertView.findViewById(R.id.textViewChampionship);

		TeamInfoVO vo = mSource.get(position);
		
		textViewTeam.setText(vo.getName());
		textViewCoach.setText(vo.getCoach());
		textViewChampionship.setText(vo.getChampionships());

		return convertView;
	}

}
