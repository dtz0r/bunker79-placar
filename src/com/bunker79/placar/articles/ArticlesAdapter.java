package com.bunker79.placar.articles;

import java.text.ParseException;
import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.bunker79.placar.R;
import com.bunker79.placar.api.vo.ArticleVO;

public class ArticlesAdapter extends BaseAdapter {

	private Context mCtx;
	private ArrayList<ArticleVO> mSource;
	protected LayoutInflater fInflater;
	protected ArticleVO mArticle;

	public ArticlesAdapter(Context ctx, ArrayList<ArticleVO> articles) {
		mCtx = ctx;
		mSource = articles;

	}


	@Override
	public int getCount() {

		return mSource.size();
	}

	@Override
	public Object getItem(int position) {
		return mSource.get(position);
	}

	@Override
	public long getItemId(int position) {

		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		convertView = LayoutInflater.from(mCtx).inflate(R.layout.viewpagerarticle, null);

		TextView textViewArticles = (TextView) convertView.findViewById(R.id.textViewArticles);
		TextView textViewData = (TextView) convertView.findViewById(R.id.textViewData);
		TextView textViewUrl = (TextView) convertView.findViewById(R.id.textViewUrl);

		ArticleVO vo = mSource.get(position);
		textViewArticles.setText(vo.getTitulo());
		try {
			textViewData.setText(vo.getFormatedData());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		textViewUrl.setText(vo.getArticleUrl());

		return convertView;
	}

}
