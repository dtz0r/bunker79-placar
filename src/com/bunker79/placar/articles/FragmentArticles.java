package com.bunker79.placar.articles;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;

import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bunker79.placar.R;
import com.bunker79.placar.api.vo.ArticleVO;
import com.bunker79.placar.api.vo.MatchVO;
import com.bunker79.placar.api.vo.TeamInfoVO;
import com.bunker79.placar.data.DataController;
import com.bunker79.placar.dispatcher.DataEvent;
import com.bunker79.placar.dispatcher.Event;
import com.bunker79.placar.dispatcher.EventListener;

public class FragmentArticles extends Fragment {

	private ListView mListView;
	private TextView mSectionTitle;
	private ArticlesAdapter mAdapter;
	private TeamsAdapter mTeamAdapter;
	private MatchesAdapter mMatchesAdapter;

	private String[] drawerListViewItems;
	private ListView drawerListView;
	private DrawerLayout drawerLayout;
	private ActionBarDrawerToggle actionBarDrawerToggle;


	private EventListener mLoadedListener = new EventListener() {

		@Override
		public void onEvent(Event event) {
			populateViewArticles();
		}

	};
	
	


	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

	}

	@Override
	public void onPause() {
		super.onPause();

		DataController.getInstance(getActivity()).removeListener(DataEvent.ARTICLES_LOADED, mLoadedListener);

	}

	@Override
	public void onStop() {
		super.onStop();

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View v = inflater.inflate(R.layout.fragment_articles, null);
		
		View btnOpenDrawer = (View) v.findViewById(R.id.btnOpenDrawer);
		
		 mSectionTitle = (TextView) v.findViewById(R.id.textViewTitle);

		drawerListViewItems = getResources().getStringArray(R.array.items);

		drawerListView = (ListView) v.findViewById(R.id.left_drawer);

		drawerListView.setAdapter(new ArrayAdapter<String>(getActivity(),
				R.layout.drawer_listview_item, drawerListViewItems));

		drawerListView.setOnItemClickListener(new DrawerItemClickListener());

		drawerLayout = (DrawerLayout) v.findViewById(R.id.drawer_layout);
		
		btnOpenDrawer.setOnClickListener(new OnClickListener() {
			
			int drawerState = 0;
			
			@Override
			public void onClick(View v) {
				
				if(drawerState == 0){
				drawerLayout.openDrawer(drawerListView);
				drawerState = 1;
				} else {
				drawerLayout.closeDrawer(drawerListView);	
				drawerState = 0;
				}
			}
		});

		mListView = (ListView) v.findViewById(R.id.articlesListView);

		return v;
	}


	@Override
	public void onResume() {
		super.onResume();

		DataController.getInstance(getActivity()).addListener(DataEvent.ARTICLES_LOADED, mLoadedListener);

		populateViewArticles();


	}

	private void populateViewArticles() {

		mSectionTitle.setText(R.string.Bar_Title_Articles);

		DataController dc = DataController.getInstance(getActivity());

		if(dc.getIsArticlesLoaded()){

			getView().findViewById(R.id.loadingarticles).setVisibility(View.GONE);

			ArticleVO[] articles =  dc.getArticles();

			ArrayList<ArticleVO> articlesList = new ArrayList<ArticleVO>();

			for (ArticleVO vo: articles){

				articlesList.add(vo);

			}

			mAdapter = new ArticlesAdapter(getActivity(), articlesList);

			mListView.setAdapter(mAdapter);
		}



	}

	private void populateViewArticlesDay() {

		mSectionTitle.setText(R.string.Bar_Title_Articles_Day);

		DataController dc = DataController.getInstance(getActivity());

		if(dc.getIsArticlesLoaded()){

			getView().findViewById(R.id.loadingarticles).setVisibility(View.GONE);

			ArticleVO[] articles =  dc.getArticles();

			ArrayList<ArticleVO> articlesList = new ArrayList<ArticleVO>();

			for (ArticleVO vo: articles){
				Calendar eventMonth = Calendar.getInstance();
				Calendar yearMonth = Calendar.getInstance();
				try {
					eventMonth.setTime(vo.getDate());
					yearMonth.set(2013, Calendar.AUGUST, Calendar.DAY_OF_MONTH);

					if(eventMonth.get(Calendar.MONTH) == yearMonth.get(Calendar.MONTH)){
						articlesList.add(vo);

					}

				} catch (ParseException e) {

					e.printStackTrace();
				}
			}

			mAdapter = new ArticlesAdapter(getActivity(), articlesList);

			mListView.setAdapter(mAdapter);
		}	

	}

	private void populateViewArticlesWeek() {

		mSectionTitle.setText(R.string.Bar_Title_Articles_Week);

		DataController dc = DataController.getInstance(getActivity());

		if(dc.getIsArticlesLoaded()){

			getView().findViewById(R.id.loadingarticles).setVisibility(View.GONE);

			ArticleVO[] articles =  dc.getArticles();

			ArrayList<ArticleVO> articlesList = new ArrayList<ArticleVO>();

			for (ArticleVO vo: articles){
				Calendar eventMonth = Calendar.getInstance();
				Calendar yearMonth = Calendar.getInstance();
				try {
					eventMonth.setTime(vo.getDate());
					yearMonth.set(2013, Calendar.JULY, Calendar.DAY_OF_MONTH);
					if(eventMonth.get(Calendar.MONTH) == yearMonth.get(Calendar.MONTH)){
						articlesList.add(vo);
					}

				} catch (ParseException e) {

					e.printStackTrace();
				}
			}

			mAdapter = new ArticlesAdapter(getActivity(), articlesList);

			mListView.setAdapter(mAdapter);
		}	

	}

	private void populateViewTeams() {

		mSectionTitle.setText(R.string.Bar_Title_Teams);

		DataController dc = DataController.getInstance(getActivity());

		if(dc.getIsTeamsLoaded()){

			TeamInfoVO[] teams = dc.getTeams();

			ArrayList<TeamInfoVO> teamsList = new ArrayList<TeamInfoVO>();

			for (TeamInfoVO vo: teams){

				teamsList.add(vo);
			}

			mTeamAdapter = new TeamsAdapter(getActivity(), teamsList);

			mListView.setAdapter(mTeamAdapter);
		}

	}
	
	private void populateViewMatches() {
		
		mSectionTitle.setText(R.string.Bar_Title_Matches);

		DataController dc = DataController.getInstance(getActivity());

		if(dc.getIsMatchesLoaded()){

			MatchVO[] matches = dc.getMatches();

			ArrayList<MatchVO> matchesList = new ArrayList<MatchVO>();

			for (MatchVO vo: matches){

				matchesList.add(vo);
			}

			mMatchesAdapter = new MatchesAdapter(getActivity(), matchesList);

			mListView.setAdapter(mMatchesAdapter);
		}
		
	}

	private class DrawerItemClickListener implements ListView.OnItemClickListener {
		@Override
		public void onItemClick(AdapterView parent, View view, int position, long id) {

			switch (position) {
			case 0:
				Toast.makeText(getActivity(), (R.string.Articles), Toast.LENGTH_SHORT).show();
				populateViewArticles();
				break;
			case 1:
				Toast.makeText(getActivity(), (R.string.Articles_Day), Toast.LENGTH_SHORT).show();
				populateViewArticlesDay();
				break;
			case 2:
				Toast.makeText(getActivity(), (R.string.Articles_Week), Toast.LENGTH_SHORT).show();
				populateViewArticlesWeek();
				break;
			case 3:
				Toast.makeText(getActivity(), (R.string.Teams), Toast.LENGTH_SHORT).show();
				populateViewTeams();
				break;
			case 4:
				Toast.makeText(getActivity(), (R.string.Matches), Toast.LENGTH_SHORT).show();
				populateViewMatches();
				break;
			default:
				break;
			}

			drawerLayout.closeDrawer(drawerListView);

		}



	}

}
