package com.bunker79.placar.articles;

import java.text.ParseException;
import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.bunker79.placar.R;
import com.bunker79.placar.api.vo.MatchVO;
import com.bunker79.placar.api.vo.TeamInfoVO;

public class MatchesAdapter extends BaseAdapter {

	private Context mCtx;
	private ArrayList<MatchVO> mSource;
	protected LayoutInflater fInflater;

	public MatchesAdapter(Context ctx, ArrayList<MatchVO> matchesList) {
		mCtx = ctx;
		mSource = matchesList;

	}


	@Override
	public int getCount() {

		return mSource.size();
	}

	@Override
	public Object getItem(int position) {
		return mSource.get(position);
	}

	@Override
	public long getItemId(int position) {

		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		convertView = LayoutInflater.from(mCtx).inflate(R.layout.viewpagermatches, null);

		TextView textViewHomeTeam = (TextView) convertView.findViewById(R.id.TextViewHomeTeam);
		TextView textViewVisitorTeam = (TextView) convertView.findViewById(R.id.textViewVisitorTeam);
		TextView textViewMatchResult = (TextView) convertView.findViewById(R.id.textViewMatchResult);
		TextView textViewDate = (TextView) convertView.findViewById(R.id.textViewDate);

		MatchVO vo = mSource.get(position);
		
		textViewHomeTeam.setText(vo.getHomeTeam().getName());
		textViewVisitorTeam.setText(vo.getVisitorTeam().getName());
		textViewMatchResult.setText(vo.getMatchResult());
	    textViewDate.setText(vo.getDate());
	    
		return convertView;
	}

}
