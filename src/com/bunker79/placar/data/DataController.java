package com.bunker79.placar.data;


import android.content.Context;

import com.bunker79.placar.api.TaskGetArticles;
import com.bunker79.placar.api.TaskGetArticles.ArticlesVOListener;
import com.bunker79.placar.api.TaskGetMatches;
import com.bunker79.placar.api.TaskGetMatches.MatchesVOListener;
import com.bunker79.placar.api.TaskGetTeams;
import com.bunker79.placar.api.TaskGetTeams.TeamsVOListener;
import com.bunker79.placar.api.vo.ArticleResponse;
import com.bunker79.placar.api.vo.ArticleVO;
import com.bunker79.placar.api.vo.MatchVO;
import com.bunker79.placar.api.vo.MatchesResponse;
import com.bunker79.placar.api.vo.TeamInfoVO;
import com.bunker79.placar.api.vo.TeamResponse;
import com.bunker79.placar.dispatcher.DataEvent;
import com.bunker79.placar.dispatcher.EventDispatcher;


public class DataController extends EventDispatcher {

	private static DataController instance;
	static final Object sLock = new Object();
	private Context mCtx;

	private LoadStatus isArticlesLoaded;
	private LoadStatus isTeamsLoaded;
	private LoadStatus isMatchesLoaded;

	private ArticleVO[] mArticles;
	private TeamInfoVO[] mTeams;
	private MatchVO[] mMatches;


	private enum LoadStatus {
		NOT_LOADED,
		LOADED,
		ERROR,
		LOADING
	}


	private ArticlesVOListener mArticlesListener = new ArticlesVOListener() {

		@Override
		public void eventosLoaded(ArticleResponse result) {
			if(result != null && result.getArticles() != null){
				mArticles = result.getArticles();

				isArticlesLoaded = LoadStatus.LOADED;
			} else {
				isArticlesLoaded = LoadStatus.ERROR;
			}
			dispatchEvent(new DataEvent(DataEvent.ARTICLES_LOADED, 0));

		}
	};

	private TeamsVOListener mTeamsListener = new TeamsVOListener() {

		@Override
		public void eventosLoaded(TeamResponse result) {
			if(result != null  && result.getTeams() != null){
				mTeams = result.getTeams();

				isTeamsLoaded = LoadStatus.LOADED;
			} else {
				isTeamsLoaded = LoadStatus.ERROR;
			}
			dispatchEvent(new DataEvent(DataEvent.TEAMS_LOADED, 0));

		}
	};
	
	private MatchesVOListener mMatchesListener = new MatchesVOListener() {

		@Override
		public void eventosLoaded(MatchesResponse result) {
			if(result != null  && result.getMatches() != null){
				mMatches = result.getMatches();

				isMatchesLoaded = LoadStatus.LOADED;
			} else {
				isMatchesLoaded = LoadStatus.ERROR;
			}
			dispatchEvent(new DataEvent(DataEvent.TEAMS_LOADED, 0));

		}
	};

	public static DataController getInstance(Context ctx) {
		synchronized (sLock) {
			if (instance == null) {
				instance = new DataController(ctx.getApplicationContext());
			}
			return instance;
		}
	}

	public DataController(Context ctx) {
		mCtx = ctx;

		isArticlesLoaded = LoadStatus.NOT_LOADED;
		isTeamsLoaded = LoadStatus.NOT_LOADED;
		isMatchesLoaded =  LoadStatus.NOT_LOADED;
		
		loadData();

	}

	private void loadData() {
		loadArticles();
		loadTeams();
		loadMatches();

	}

	public void loadArticles() {

		if(isArticlesLoaded!=LoadStatus.LOADING) {
			isArticlesLoaded = LoadStatus.LOADING;
			TaskGetArticles task = new TaskGetArticles();
			task.setListener(mArticlesListener);
			task.execute();
		}
	}

	public void loadTeams() {

		if(isTeamsLoaded!=LoadStatus.LOADING) {
			isTeamsLoaded = LoadStatus.LOADING;
			TaskGetTeams task = new TaskGetTeams();
			task.setListener(mTeamsListener);
			task.execute();
		}
	}
	
	public void loadMatches() {

		if(isMatchesLoaded!=LoadStatus.LOADING) {
			isMatchesLoaded = LoadStatus.LOADING;
			TaskGetMatches task = new TaskGetMatches();
			task.setListener(mMatchesListener);
			task.execute();
		}
	}

	public ArticleVO[] getArticles(){

		return mArticles;
	}
	
	public TeamInfoVO[] getTeams(){

		return mTeams;
	}
	
	public MatchVO[] getMatches(){

		return mMatches;
	}

	public boolean getIsArticlesLoaded() {
		return isArticlesLoaded==LoadStatus.LOADED;
	}
	
	public boolean getIsTeamsLoaded() {
		return isTeamsLoaded==LoadStatus.LOADED;
	}
	
	public boolean getIsMatchesLoaded() {
		return isMatchesLoaded==LoadStatus.LOADED;
	}

	public boolean getIsArticlesFailed() {
		return isArticlesLoaded==LoadStatus.ERROR;
	}
	
	public boolean getIsTeamsFailed() {
		return isTeamsLoaded==LoadStatus.ERROR;
	}
	
	public boolean getIsMatchesFailed() {
		return isMatchesLoaded==LoadStatus.ERROR;
	}

}

